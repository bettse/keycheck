
#include "mifare.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <getopt.h>
#include <nfc/nfc.h>

#define SECTOR_COUNT 16
#define MFC_KEYLEN 6
#define UID_LEN 4

void printHex(uint8_t *buffer, int len) {
  for(int i = 0; i < len; i++) {
    printf("%02x", buffer[i]);
  }
}

int main(int argc, char **argv) {
  nfc_context *context;
  nfc_device *pnd;
  nfc_target nt;
  mifare_param mp;
  int c;
  uint8_t keya[MFC_KEYLEN] = {0};
  int sector = 0;

  while ((c = getopt (argc, argv, "k:")) != -1) {
    switch (c) {
      case 'k':
        sscanf(optarg, "%2x%2x%2x%2x%2x%2x", &keya[0], &keya[1], &keya[2], &keya[3], &keya[4], &keya[5]);
        break;
      case '?':
        break;
      default:
        abort();
    }
  }
  for (int index = optind; index < argc; index++) {
    printf ("Non-option argument %s\n", argv[index]);
  }

  nfc_init(&context);
  if (context == NULL) {
    printf("Unable to init libnfc (malloc)\n");
    exit(EXIT_FAILURE);
  }

  pnd = nfc_open(context, NULL);

  if (pnd == NULL) {
    printf("ERROR: %s\n", "Unable to open NFC device.");
    exit(EXIT_FAILURE);
  }
  if (nfc_initiator_init(pnd) < 0) {
    nfc_perror(pnd, "nfc_initiator_init");
    exit(EXIT_FAILURE);
  }

  const nfc_modulation nmMifare = {
    .nmt = NMT_ISO14443A,
    .nbr = NBR_106,
  };

  if (nfc_initiator_select_passive_target(pnd, nmMifare, NULL, 0, &nt) <= 0) {
    printf("Error: no tag was found\n");
    nfc_close(pnd);
    nfc_exit(context);
    exit(EXIT_FAILURE);
  }

  if ((nt.nti.nai.btSak & 0x08) == 0 && (nt.nti.nai.btSak & 0x01) == 0) {
    printf("Warning: tag is probably not Mifare Classic!\n");
    exit(EXIT_FAILURE);
  }

  int trailer = sector * 4 + 3;
  printf("UID "); printHex(nt.nti.nai.abtUid, nt.nti.nai.szUidLen); printf("\n");
  printf("trailer for sector %u: %u\n", sector, trailer);
  printf("KeyA: "); printHex(keya, MFC_KEYLEN); printf("\n");

  // Set the authentication information (uid)
  memcpy(mp.mpa.abtAuthUid, nt.nti.nai.abtUid + nt.nti.nai.szUidLen - 4, UID_LEN);
  memcpy(mp.mpd.abtData, keya, MFC_KEYLEN);

  if (nfc_initiator_mifare_cmd(pnd, MC_AUTH_A, trailer, &mp)) { //Auth
    printf("Authenticated sector %i\n", sector);
  } else {
    printf("Failed to auth sector %x\n", sector);
  }

  if (nfc_initiator_select_passive_target(pnd, nmMifare, nt.nti.nai.abtUid, nt.nti.nai.szUidLen, NULL) <= 0) {
    printf("tag was removed");
  }


  nfc_close(pnd);
  nfc_exit(context);
  exit(EXIT_SUCCESS);
}

