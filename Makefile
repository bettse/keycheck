
all: keycheck

keycheck: mifare.o main.o
	gcc -std=c99 *.o -lnfc -o keycheck

mifare.o: mifare.c
	gcc -std=c99 -c mifare.c

main.o: main.c
	gcc -std=c99 -c main.c

clean:
	rm *o keycheck

